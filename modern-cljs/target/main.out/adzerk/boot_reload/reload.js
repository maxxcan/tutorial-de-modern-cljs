// Compiled by ClojureScript 1.10.238 {}
goog.provide('adzerk.boot_reload.reload');
goog.require('cljs.core');
goog.require('clojure.string');
goog.require('goog.Uri');
goog.require('goog.async.DeferredList');
goog.require('goog.html.legacyconversions');
goog.require('goog.net.jsloader');
adzerk.boot_reload.reload.page_uri = (function adzerk$boot_reload$reload$page_uri(){
return (new goog.Uri(window.location.href));
});
adzerk.boot_reload.reload.ends_with_QMARK_ = (function adzerk$boot_reload$reload$ends_with_QMARK_(s,pat){
return cljs.core._EQ_.call(null,pat,cljs.core.subs.call(null,s,(cljs.core.count.call(null,s) - cljs.core.count.call(null,pat))));
});
adzerk.boot_reload.reload.reload_page_BANG_ = (function adzerk$boot_reload$reload$reload_page_BANG_(){
return window.location.reload();
});
adzerk.boot_reload.reload.normalize_href_or_uri = (function adzerk$boot_reload$reload$normalize_href_or_uri(href_or_uri){
var uri = (new goog.Uri(href_or_uri));
return adzerk.boot_reload.reload.page_uri.call(null).resolve(uri).getPath();
});
/**
 * Produce the changed goog.Uri iff the (relative) path is different
 *   compared to the content of changed (a string). Return nil otherwise.
 */
adzerk.boot_reload.reload.changed_uri = (function adzerk$boot_reload$reload$changed_uri(href_or_uri,changed){
if(cljs.core.truth_(href_or_uri)){
var path = adzerk.boot_reload.reload.normalize_href_or_uri.call(null,href_or_uri);
var temp__5720__auto__ = cljs.core.first.call(null,cljs.core.filter.call(null,((function (path){
return (function (p1__1212_SHARP_){
return adzerk.boot_reload.reload.ends_with_QMARK_.call(null,adzerk.boot_reload.reload.normalize_href_or_uri.call(null,p1__1212_SHARP_),path);
});})(path))
,changed));
if(cljs.core.truth_(temp__5720__auto__)){
var changed__$1 = temp__5720__auto__;
return goog.Uri.parse(changed__$1);
} else {
return null;
}
} else {
return null;
}
});
adzerk.boot_reload.reload.reload_css = (function adzerk$boot_reload$reload$reload_css(changed){
var sheets = document.styleSheets;
var seq__1213 = cljs.core.seq.call(null,cljs.core.range.call(null,(0),sheets.length));
var chunk__1214 = null;
var count__1215 = (0);
var i__1216 = (0);
while(true){
if((i__1216 < count__1215)){
var s = cljs.core._nth.call(null,chunk__1214,i__1216);
var temp__5720__auto___1217 = (sheets[s]);
if(cljs.core.truth_(temp__5720__auto___1217)){
var sheet_1218 = temp__5720__auto___1217;
var temp__5720__auto___1219__$1 = adzerk.boot_reload.reload.changed_uri.call(null,sheet_1218.href,changed);
if(cljs.core.truth_(temp__5720__auto___1219__$1)){
var href_uri_1220 = temp__5720__auto___1219__$1;
sheet_1218.ownerNode.href = href_uri_1220.makeUnique().toString();
} else {
}
} else {
}


var G__1221 = seq__1213;
var G__1222 = chunk__1214;
var G__1223 = count__1215;
var G__1224 = (i__1216 + (1));
seq__1213 = G__1221;
chunk__1214 = G__1222;
count__1215 = G__1223;
i__1216 = G__1224;
continue;
} else {
var temp__5720__auto__ = cljs.core.seq.call(null,seq__1213);
if(temp__5720__auto__){
var seq__1213__$1 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__1213__$1)){
var c__4319__auto__ = cljs.core.chunk_first.call(null,seq__1213__$1);
var G__1225 = cljs.core.chunk_rest.call(null,seq__1213__$1);
var G__1226 = c__4319__auto__;
var G__1227 = cljs.core.count.call(null,c__4319__auto__);
var G__1228 = (0);
seq__1213 = G__1225;
chunk__1214 = G__1226;
count__1215 = G__1227;
i__1216 = G__1228;
continue;
} else {
var s = cljs.core.first.call(null,seq__1213__$1);
var temp__5720__auto___1229__$1 = (sheets[s]);
if(cljs.core.truth_(temp__5720__auto___1229__$1)){
var sheet_1230 = temp__5720__auto___1229__$1;
var temp__5720__auto___1231__$2 = adzerk.boot_reload.reload.changed_uri.call(null,sheet_1230.href,changed);
if(cljs.core.truth_(temp__5720__auto___1231__$2)){
var href_uri_1232 = temp__5720__auto___1231__$2;
sheet_1230.ownerNode.href = href_uri_1232.makeUnique().toString();
} else {
}
} else {
}


var G__1233 = cljs.core.next.call(null,seq__1213__$1);
var G__1234 = null;
var G__1235 = (0);
var G__1236 = (0);
seq__1213 = G__1233;
chunk__1214 = G__1234;
count__1215 = G__1235;
i__1216 = G__1236;
continue;
}
} else {
return null;
}
}
break;
}
});
adzerk.boot_reload.reload.reload_img = (function adzerk$boot_reload$reload$reload_img(changed){
var images = document.images;
var seq__1237 = cljs.core.seq.call(null,cljs.core.range.call(null,(0),images.length));
var chunk__1238 = null;
var count__1239 = (0);
var i__1240 = (0);
while(true){
if((i__1240 < count__1239)){
var s = cljs.core._nth.call(null,chunk__1238,i__1240);
var temp__5720__auto___1241 = (images[s]);
if(cljs.core.truth_(temp__5720__auto___1241)){
var image_1242 = temp__5720__auto___1241;
var temp__5720__auto___1243__$1 = adzerk.boot_reload.reload.changed_uri.call(null,image_1242.src,changed);
if(cljs.core.truth_(temp__5720__auto___1243__$1)){
var href_uri_1244 = temp__5720__auto___1243__$1;
image_1242.src = href_uri_1244.makeUnique().toString();
} else {
}
} else {
}


var G__1245 = seq__1237;
var G__1246 = chunk__1238;
var G__1247 = count__1239;
var G__1248 = (i__1240 + (1));
seq__1237 = G__1245;
chunk__1238 = G__1246;
count__1239 = G__1247;
i__1240 = G__1248;
continue;
} else {
var temp__5720__auto__ = cljs.core.seq.call(null,seq__1237);
if(temp__5720__auto__){
var seq__1237__$1 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__1237__$1)){
var c__4319__auto__ = cljs.core.chunk_first.call(null,seq__1237__$1);
var G__1249 = cljs.core.chunk_rest.call(null,seq__1237__$1);
var G__1250 = c__4319__auto__;
var G__1251 = cljs.core.count.call(null,c__4319__auto__);
var G__1252 = (0);
seq__1237 = G__1249;
chunk__1238 = G__1250;
count__1239 = G__1251;
i__1240 = G__1252;
continue;
} else {
var s = cljs.core.first.call(null,seq__1237__$1);
var temp__5720__auto___1253__$1 = (images[s]);
if(cljs.core.truth_(temp__5720__auto___1253__$1)){
var image_1254 = temp__5720__auto___1253__$1;
var temp__5720__auto___1255__$2 = adzerk.boot_reload.reload.changed_uri.call(null,image_1254.src,changed);
if(cljs.core.truth_(temp__5720__auto___1255__$2)){
var href_uri_1256 = temp__5720__auto___1255__$2;
image_1254.src = href_uri_1256.makeUnique().toString();
} else {
}
} else {
}


var G__1257 = cljs.core.next.call(null,seq__1237__$1);
var G__1258 = null;
var G__1259 = (0);
var G__1260 = (0);
seq__1237 = G__1257;
chunk__1238 = G__1258;
count__1239 = G__1259;
i__1240 = G__1260;
continue;
}
} else {
return null;
}
}
break;
}
});
adzerk.boot_reload.reload.load_files = (function adzerk$boot_reload$reload$load_files(urls){
var opts = ({"cleanupWhenDone": true});
if(typeof goog.net.jsloader.safeLoadMany !== 'undefined'){
return goog.net.jsloader.safeLoadMany(cljs.core.clj__GT_js.call(null,cljs.core.map.call(null,((function (opts){
return (function (p1__1261_SHARP_){
return goog.html.legacyconversions.trustedResourceUrlFromString(p1__1261_SHARP_.toString());
});})(opts))
,urls)),opts);
} else {
if(typeof goog.net.jsloader.loadMany !== 'undefined'){
return goog.net.jsloader.loadMany(cljs.core.clj__GT_js.call(null,urls),opts);
} else {
throw cljs.core.ex_info.call(null,"jsloader/safeLoadMany not found",cljs.core.PersistentArrayMap.EMPTY);

}
}
});
adzerk.boot_reload.reload.reload_js = (function adzerk$boot_reload$reload$reload_js(changed,p__1264){
var map__1265 = p__1264;
var map__1265__$1 = ((((!((map__1265 == null)))?(((((map__1265.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__1265.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__1265):map__1265);
var on_jsload = cljs.core.get.call(null,map__1265__$1,new cljs.core.Keyword(null,"on-jsload","on-jsload",-395756602),cljs.core.identity);
var js_files = cljs.core.filter.call(null,((function (map__1265,map__1265__$1,on_jsload){
return (function (p1__1262_SHARP_){
return adzerk.boot_reload.reload.ends_with_QMARK_.call(null,p1__1262_SHARP_,".js");
});})(map__1265,map__1265__$1,on_jsload))
,changed);
if(cljs.core.seq.call(null,js_files)){
adzerk.boot_reload.reload.load_files.call(null,cljs.core.map.call(null,((function (js_files,map__1265,map__1265__$1,on_jsload){
return (function (p1__1263_SHARP_){
return goog.Uri.parse(p1__1263_SHARP_).makeUnique();
});})(js_files,map__1265,map__1265__$1,on_jsload))
,js_files)).addCallbacks(((function (js_files,map__1265,map__1265__$1,on_jsload){
return (function() { 
var G__1267__delegate = function (_){
return on_jsload.call(null);
};
var G__1267 = function (var_args){
var _ = null;
if (arguments.length > 0) {
var G__1268__i = 0, G__1268__a = new Array(arguments.length -  0);
while (G__1268__i < G__1268__a.length) {G__1268__a[G__1268__i] = arguments[G__1268__i + 0]; ++G__1268__i;}
  _ = new cljs.core.IndexedSeq(G__1268__a,0,null);
} 
return G__1267__delegate.call(this,_);};
G__1267.cljs$lang$maxFixedArity = 0;
G__1267.cljs$lang$applyTo = (function (arglist__1269){
var _ = cljs.core.seq(arglist__1269);
return G__1267__delegate(_);
});
G__1267.cljs$core$IFn$_invoke$arity$variadic = G__1267__delegate;
return G__1267;
})()
;})(js_files,map__1265,map__1265__$1,on_jsload))
,((function (js_files,map__1265,map__1265__$1,on_jsload){
return (function (e){
return console.error("Load failed:",e.message);
});})(js_files,map__1265,map__1265__$1,on_jsload))
);

if(cljs.core.truth_((window["jQuery"]))){
return jQuery(document).trigger("page-load");
} else {
return null;
}
} else {
return null;
}
});
adzerk.boot_reload.reload.reload_html = (function adzerk$boot_reload$reload$reload_html(changed){
var page_path = adzerk.boot_reload.reload.page_uri.call(null).getPath();
var html_path = (cljs.core.truth_(adzerk.boot_reload.reload.ends_with_QMARK_.call(null,page_path,"/"))?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(page_path),"index.html"].join(''):page_path);
if(cljs.core.truth_(adzerk.boot_reload.reload.changed_uri.call(null,html_path,changed))){
return adzerk.boot_reload.reload.reload_page_BANG_.call(null);
} else {
return null;
}
});
adzerk.boot_reload.reload.group_log = (function adzerk$boot_reload$reload$group_log(title,things_to_log){
console.groupCollapsed(title);

var seq__1270_1274 = cljs.core.seq.call(null,things_to_log);
var chunk__1271_1275 = null;
var count__1272_1276 = (0);
var i__1273_1277 = (0);
while(true){
if((i__1273_1277 < count__1272_1276)){
var t_1278 = cljs.core._nth.call(null,chunk__1271_1275,i__1273_1277);
console.log(t_1278);


var G__1279 = seq__1270_1274;
var G__1280 = chunk__1271_1275;
var G__1281 = count__1272_1276;
var G__1282 = (i__1273_1277 + (1));
seq__1270_1274 = G__1279;
chunk__1271_1275 = G__1280;
count__1272_1276 = G__1281;
i__1273_1277 = G__1282;
continue;
} else {
var temp__5720__auto___1283 = cljs.core.seq.call(null,seq__1270_1274);
if(temp__5720__auto___1283){
var seq__1270_1284__$1 = temp__5720__auto___1283;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__1270_1284__$1)){
var c__4319__auto___1285 = cljs.core.chunk_first.call(null,seq__1270_1284__$1);
var G__1286 = cljs.core.chunk_rest.call(null,seq__1270_1284__$1);
var G__1287 = c__4319__auto___1285;
var G__1288 = cljs.core.count.call(null,c__4319__auto___1285);
var G__1289 = (0);
seq__1270_1274 = G__1286;
chunk__1271_1275 = G__1287;
count__1272_1276 = G__1288;
i__1273_1277 = G__1289;
continue;
} else {
var t_1290 = cljs.core.first.call(null,seq__1270_1284__$1);
console.log(t_1290);


var G__1291 = cljs.core.next.call(null,seq__1270_1284__$1);
var G__1292 = null;
var G__1293 = (0);
var G__1294 = (0);
seq__1270_1274 = G__1291;
chunk__1271_1275 = G__1292;
count__1272_1276 = G__1293;
i__1273_1277 = G__1294;
continue;
}
} else {
}
}
break;
}

return console.groupEnd();
});
/**
 * Perform heuristics to check if a non-shimmed DOM is available
 */
adzerk.boot_reload.reload.has_dom_QMARK_ = (function adzerk$boot_reload$reload$has_dom_QMARK_(){
return ((typeof window !== 'undefined') && (typeof window.document !== 'undefined') && (typeof window.document.documentURI !== 'undefined'));
});
adzerk.boot_reload.reload.reload = (function adzerk$boot_reload$reload$reload(changed,opts){
var changed_STAR_ = cljs.core.map.call(null,(function (p1__1295_SHARP_){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"asset-host","asset-host",-899289050).cljs$core$IFn$_invoke$arity$1(opts)),cljs.core.str.cljs$core$IFn$_invoke$arity$1(p1__1295_SHARP_)].join('');
}),cljs.core.map.call(null,(function (p__1296){
var map__1297 = p__1296;
var map__1297__$1 = ((((!((map__1297 == null)))?(((((map__1297.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__1297.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__1297):map__1297);
var canonical_path = cljs.core.get.call(null,map__1297__$1,new cljs.core.Keyword(null,"canonical-path","canonical-path",-1891747854));
var web_path = cljs.core.get.call(null,map__1297__$1,new cljs.core.Keyword(null,"web-path","web-path",594590673));
if(cljs.core._EQ_.call(null,"file:",(function (){var G__1299 = window;
var G__1299__$1 = (((G__1299 == null))?null:G__1299.location);
if((G__1299__$1 == null)){
return null;
} else {
return G__1299__$1.protocol;
}
})())){
return canonical_path;
} else {
return web_path;
}
}),changed));
adzerk.boot_reload.reload.group_log.call(null,"Reload",changed_STAR_);

adzerk.boot_reload.reload.reload_js.call(null,changed_STAR_,opts);

if(cljs.core.truth_(adzerk.boot_reload.reload.has_dom_QMARK_.call(null))){
var G__1300 = changed_STAR_;
adzerk.boot_reload.reload.reload_html.call(null,G__1300);

adzerk.boot_reload.reload.reload_css.call(null,G__1300);

adzerk.boot_reload.reload.reload_img.call(null,G__1300);

return G__1300;
} else {
return null;
}
});

//# sourceMappingURL=reload.js.map
