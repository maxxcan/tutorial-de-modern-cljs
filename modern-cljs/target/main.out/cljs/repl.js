// Compiled by ClojureScript 1.10.238 {}
goog.provide('cljs.repl');
goog.require('cljs.core');
goog.require('cljs.spec.alpha');
cljs.repl.print_doc = (function cljs$repl$print_doc(p__2569){
var map__2570 = p__2569;
var map__2570__$1 = ((((!((map__2570 == null)))?(((((map__2570.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__2570.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__2570):map__2570);
var m = map__2570__$1;
var n = cljs.core.get.call(null,map__2570__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
var nm = cljs.core.get.call(null,map__2570__$1,new cljs.core.Keyword(null,"name","name",1843675177));
cljs.core.println.call(null,"-------------------------");

cljs.core.println.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var temp__5720__auto__ = new cljs.core.Keyword(null,"ns","ns",441598760).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(temp__5720__auto__)){
var ns = temp__5720__auto__;
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(ns),"/"].join('');
} else {
return null;
}
})()),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join(''));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Protocol");
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m))){
var seq__2572_2594 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m));
var chunk__2573_2595 = null;
var count__2574_2596 = (0);
var i__2575_2597 = (0);
while(true){
if((i__2575_2597 < count__2574_2596)){
var f_2598 = cljs.core._nth.call(null,chunk__2573_2595,i__2575_2597);
cljs.core.println.call(null,"  ",f_2598);


var G__2599 = seq__2572_2594;
var G__2600 = chunk__2573_2595;
var G__2601 = count__2574_2596;
var G__2602 = (i__2575_2597 + (1));
seq__2572_2594 = G__2599;
chunk__2573_2595 = G__2600;
count__2574_2596 = G__2601;
i__2575_2597 = G__2602;
continue;
} else {
var temp__5720__auto___2603 = cljs.core.seq.call(null,seq__2572_2594);
if(temp__5720__auto___2603){
var seq__2572_2604__$1 = temp__5720__auto___2603;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__2572_2604__$1)){
var c__4319__auto___2605 = cljs.core.chunk_first.call(null,seq__2572_2604__$1);
var G__2606 = cljs.core.chunk_rest.call(null,seq__2572_2604__$1);
var G__2607 = c__4319__auto___2605;
var G__2608 = cljs.core.count.call(null,c__4319__auto___2605);
var G__2609 = (0);
seq__2572_2594 = G__2606;
chunk__2573_2595 = G__2607;
count__2574_2596 = G__2608;
i__2575_2597 = G__2609;
continue;
} else {
var f_2610 = cljs.core.first.call(null,seq__2572_2604__$1);
cljs.core.println.call(null,"  ",f_2610);


var G__2611 = cljs.core.next.call(null,seq__2572_2604__$1);
var G__2612 = null;
var G__2613 = (0);
var G__2614 = (0);
seq__2572_2594 = G__2611;
chunk__2573_2595 = G__2612;
count__2574_2596 = G__2613;
i__2575_2597 = G__2614;
continue;
}
} else {
}
}
break;
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m))){
var arglists_2615 = new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_((function (){var or__3922__auto__ = new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__3922__auto__)){
return or__3922__auto__;
} else {
return new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m);
}
})())){
cljs.core.prn.call(null,arglists_2615);
} else {
cljs.core.prn.call(null,((cljs.core._EQ_.call(null,new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.first.call(null,arglists_2615)))?cljs.core.second.call(null,arglists_2615):arglists_2615));
}
} else {
}
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"special-form","special-form",-1326536374).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Special Form");

cljs.core.println.call(null," ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m));

if(cljs.core.contains_QMARK_.call(null,m,new cljs.core.Keyword(null,"url","url",276297046))){
if(cljs.core.truth_(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))){
return cljs.core.println.call(null,["\n  Please see http://clojure.org/",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))].join(''));
} else {
return null;
}
} else {
return cljs.core.println.call(null,["\n  Please see http://clojure.org/special_forms#",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join(''));
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Macro");
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"REPL Special Function");
} else {
}

cljs.core.println.call(null," ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
var seq__2576_2616 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"methods","methods",453930866).cljs$core$IFn$_invoke$arity$1(m));
var chunk__2577_2617 = null;
var count__2578_2618 = (0);
var i__2579_2619 = (0);
while(true){
if((i__2579_2619 < count__2578_2618)){
var vec__2580_2620 = cljs.core._nth.call(null,chunk__2577_2617,i__2579_2619);
var name_2621 = cljs.core.nth.call(null,vec__2580_2620,(0),null);
var map__2583_2622 = cljs.core.nth.call(null,vec__2580_2620,(1),null);
var map__2583_2623__$1 = ((((!((map__2583_2622 == null)))?(((((map__2583_2622.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__2583_2622.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__2583_2622):map__2583_2622);
var doc_2624 = cljs.core.get.call(null,map__2583_2623__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_2625 = cljs.core.get.call(null,map__2583_2623__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println.call(null);

cljs.core.println.call(null," ",name_2621);

cljs.core.println.call(null," ",arglists_2625);

if(cljs.core.truth_(doc_2624)){
cljs.core.println.call(null," ",doc_2624);
} else {
}


var G__2626 = seq__2576_2616;
var G__2627 = chunk__2577_2617;
var G__2628 = count__2578_2618;
var G__2629 = (i__2579_2619 + (1));
seq__2576_2616 = G__2626;
chunk__2577_2617 = G__2627;
count__2578_2618 = G__2628;
i__2579_2619 = G__2629;
continue;
} else {
var temp__5720__auto___2630 = cljs.core.seq.call(null,seq__2576_2616);
if(temp__5720__auto___2630){
var seq__2576_2631__$1 = temp__5720__auto___2630;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__2576_2631__$1)){
var c__4319__auto___2632 = cljs.core.chunk_first.call(null,seq__2576_2631__$1);
var G__2633 = cljs.core.chunk_rest.call(null,seq__2576_2631__$1);
var G__2634 = c__4319__auto___2632;
var G__2635 = cljs.core.count.call(null,c__4319__auto___2632);
var G__2636 = (0);
seq__2576_2616 = G__2633;
chunk__2577_2617 = G__2634;
count__2578_2618 = G__2635;
i__2579_2619 = G__2636;
continue;
} else {
var vec__2585_2637 = cljs.core.first.call(null,seq__2576_2631__$1);
var name_2638 = cljs.core.nth.call(null,vec__2585_2637,(0),null);
var map__2588_2639 = cljs.core.nth.call(null,vec__2585_2637,(1),null);
var map__2588_2640__$1 = ((((!((map__2588_2639 == null)))?(((((map__2588_2639.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__2588_2639.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__2588_2639):map__2588_2639);
var doc_2641 = cljs.core.get.call(null,map__2588_2640__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_2642 = cljs.core.get.call(null,map__2588_2640__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println.call(null);

cljs.core.println.call(null," ",name_2638);

cljs.core.println.call(null," ",arglists_2642);

if(cljs.core.truth_(doc_2641)){
cljs.core.println.call(null," ",doc_2641);
} else {
}


var G__2643 = cljs.core.next.call(null,seq__2576_2631__$1);
var G__2644 = null;
var G__2645 = (0);
var G__2646 = (0);
seq__2576_2616 = G__2643;
chunk__2577_2617 = G__2644;
count__2578_2618 = G__2645;
i__2579_2619 = G__2646;
continue;
}
} else {
}
}
break;
}
} else {
}

if(cljs.core.truth_(n)){
var temp__5720__auto__ = cljs.spec.alpha.get_spec.call(null,cljs.core.symbol.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.ns_name.call(null,n))].join(''),cljs.core.name.call(null,nm)));
if(cljs.core.truth_(temp__5720__auto__)){
var fnspec = temp__5720__auto__;
cljs.core.print.call(null,"Spec");

var seq__2590 = cljs.core.seq.call(null,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"args","args",1315556576),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Keyword(null,"fn","fn",-1175266204)], null));
var chunk__2591 = null;
var count__2592 = (0);
var i__2593 = (0);
while(true){
if((i__2593 < count__2592)){
var role = cljs.core._nth.call(null,chunk__2591,i__2593);
var temp__5720__auto___2647__$1 = cljs.core.get.call(null,fnspec,role);
if(cljs.core.truth_(temp__5720__auto___2647__$1)){
var spec_2648 = temp__5720__auto___2647__$1;
cljs.core.print.call(null,["\n ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.name.call(null,role)),":"].join(''),cljs.spec.alpha.describe.call(null,spec_2648));
} else {
}


var G__2649 = seq__2590;
var G__2650 = chunk__2591;
var G__2651 = count__2592;
var G__2652 = (i__2593 + (1));
seq__2590 = G__2649;
chunk__2591 = G__2650;
count__2592 = G__2651;
i__2593 = G__2652;
continue;
} else {
var temp__5720__auto____$1 = cljs.core.seq.call(null,seq__2590);
if(temp__5720__auto____$1){
var seq__2590__$1 = temp__5720__auto____$1;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__2590__$1)){
var c__4319__auto__ = cljs.core.chunk_first.call(null,seq__2590__$1);
var G__2653 = cljs.core.chunk_rest.call(null,seq__2590__$1);
var G__2654 = c__4319__auto__;
var G__2655 = cljs.core.count.call(null,c__4319__auto__);
var G__2656 = (0);
seq__2590 = G__2653;
chunk__2591 = G__2654;
count__2592 = G__2655;
i__2593 = G__2656;
continue;
} else {
var role = cljs.core.first.call(null,seq__2590__$1);
var temp__5720__auto___2657__$2 = cljs.core.get.call(null,fnspec,role);
if(cljs.core.truth_(temp__5720__auto___2657__$2)){
var spec_2658 = temp__5720__auto___2657__$2;
cljs.core.print.call(null,["\n ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.name.call(null,role)),":"].join(''),cljs.spec.alpha.describe.call(null,spec_2658));
} else {
}


var G__2659 = cljs.core.next.call(null,seq__2590__$1);
var G__2660 = null;
var G__2661 = (0);
var G__2662 = (0);
seq__2590 = G__2659;
chunk__2591 = G__2660;
count__2592 = G__2661;
i__2593 = G__2662;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
} else {
return null;
}
}
});

//# sourceMappingURL=repl.js.map
