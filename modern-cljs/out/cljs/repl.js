// Compiled by ClojureScript 1.10.238 {}
goog.provide('cljs.repl');
goog.require('cljs.core');
goog.require('cljs.spec.alpha');
cljs.repl.print_doc = (function cljs$repl$print_doc(p__4691){
var map__4692 = p__4691;
var map__4692__$1 = ((((!((map__4692 == null)))?(((((map__4692.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__4692.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__4692):map__4692);
var m = map__4692__$1;
var n = cljs.core.get.call(null,map__4692__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
var nm = cljs.core.get.call(null,map__4692__$1,new cljs.core.Keyword(null,"name","name",1843675177));
cljs.core.println.call(null,"-------------------------");

cljs.core.println.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var temp__5720__auto__ = new cljs.core.Keyword(null,"ns","ns",441598760).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(temp__5720__auto__)){
var ns = temp__5720__auto__;
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(ns),"/"].join('');
} else {
return null;
}
})()),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join(''));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Protocol");
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m))){
var seq__4694_4716 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m));
var chunk__4695_4717 = null;
var count__4696_4718 = (0);
var i__4697_4719 = (0);
while(true){
if((i__4697_4719 < count__4696_4718)){
var f_4720 = cljs.core._nth.call(null,chunk__4695_4717,i__4697_4719);
cljs.core.println.call(null,"  ",f_4720);


var G__4721 = seq__4694_4716;
var G__4722 = chunk__4695_4717;
var G__4723 = count__4696_4718;
var G__4724 = (i__4697_4719 + (1));
seq__4694_4716 = G__4721;
chunk__4695_4717 = G__4722;
count__4696_4718 = G__4723;
i__4697_4719 = G__4724;
continue;
} else {
var temp__5720__auto___4725 = cljs.core.seq.call(null,seq__4694_4716);
if(temp__5720__auto___4725){
var seq__4694_4726__$1 = temp__5720__auto___4725;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__4694_4726__$1)){
var c__4319__auto___4727 = cljs.core.chunk_first.call(null,seq__4694_4726__$1);
var G__4728 = cljs.core.chunk_rest.call(null,seq__4694_4726__$1);
var G__4729 = c__4319__auto___4727;
var G__4730 = cljs.core.count.call(null,c__4319__auto___4727);
var G__4731 = (0);
seq__4694_4716 = G__4728;
chunk__4695_4717 = G__4729;
count__4696_4718 = G__4730;
i__4697_4719 = G__4731;
continue;
} else {
var f_4732 = cljs.core.first.call(null,seq__4694_4726__$1);
cljs.core.println.call(null,"  ",f_4732);


var G__4733 = cljs.core.next.call(null,seq__4694_4726__$1);
var G__4734 = null;
var G__4735 = (0);
var G__4736 = (0);
seq__4694_4716 = G__4733;
chunk__4695_4717 = G__4734;
count__4696_4718 = G__4735;
i__4697_4719 = G__4736;
continue;
}
} else {
}
}
break;
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m))){
var arglists_4737 = new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_((function (){var or__3922__auto__ = new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__3922__auto__)){
return or__3922__auto__;
} else {
return new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m);
}
})())){
cljs.core.prn.call(null,arglists_4737);
} else {
cljs.core.prn.call(null,((cljs.core._EQ_.call(null,new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.first.call(null,arglists_4737)))?cljs.core.second.call(null,arglists_4737):arglists_4737));
}
} else {
}
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"special-form","special-form",-1326536374).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Special Form");

cljs.core.println.call(null," ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m));

if(cljs.core.contains_QMARK_.call(null,m,new cljs.core.Keyword(null,"url","url",276297046))){
if(cljs.core.truth_(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))){
return cljs.core.println.call(null,["\n  Please see http://clojure.org/",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))].join(''));
} else {
return null;
}
} else {
return cljs.core.println.call(null,["\n  Please see http://clojure.org/special_forms#",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join(''));
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Macro");
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"REPL Special Function");
} else {
}

cljs.core.println.call(null," ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
var seq__4698_4738 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"methods","methods",453930866).cljs$core$IFn$_invoke$arity$1(m));
var chunk__4699_4739 = null;
var count__4700_4740 = (0);
var i__4701_4741 = (0);
while(true){
if((i__4701_4741 < count__4700_4740)){
var vec__4702_4742 = cljs.core._nth.call(null,chunk__4699_4739,i__4701_4741);
var name_4743 = cljs.core.nth.call(null,vec__4702_4742,(0),null);
var map__4705_4744 = cljs.core.nth.call(null,vec__4702_4742,(1),null);
var map__4705_4745__$1 = ((((!((map__4705_4744 == null)))?(((((map__4705_4744.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__4705_4744.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__4705_4744):map__4705_4744);
var doc_4746 = cljs.core.get.call(null,map__4705_4745__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_4747 = cljs.core.get.call(null,map__4705_4745__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println.call(null);

cljs.core.println.call(null," ",name_4743);

cljs.core.println.call(null," ",arglists_4747);

if(cljs.core.truth_(doc_4746)){
cljs.core.println.call(null," ",doc_4746);
} else {
}


var G__4748 = seq__4698_4738;
var G__4749 = chunk__4699_4739;
var G__4750 = count__4700_4740;
var G__4751 = (i__4701_4741 + (1));
seq__4698_4738 = G__4748;
chunk__4699_4739 = G__4749;
count__4700_4740 = G__4750;
i__4701_4741 = G__4751;
continue;
} else {
var temp__5720__auto___4752 = cljs.core.seq.call(null,seq__4698_4738);
if(temp__5720__auto___4752){
var seq__4698_4753__$1 = temp__5720__auto___4752;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__4698_4753__$1)){
var c__4319__auto___4754 = cljs.core.chunk_first.call(null,seq__4698_4753__$1);
var G__4755 = cljs.core.chunk_rest.call(null,seq__4698_4753__$1);
var G__4756 = c__4319__auto___4754;
var G__4757 = cljs.core.count.call(null,c__4319__auto___4754);
var G__4758 = (0);
seq__4698_4738 = G__4755;
chunk__4699_4739 = G__4756;
count__4700_4740 = G__4757;
i__4701_4741 = G__4758;
continue;
} else {
var vec__4707_4759 = cljs.core.first.call(null,seq__4698_4753__$1);
var name_4760 = cljs.core.nth.call(null,vec__4707_4759,(0),null);
var map__4710_4761 = cljs.core.nth.call(null,vec__4707_4759,(1),null);
var map__4710_4762__$1 = ((((!((map__4710_4761 == null)))?(((((map__4710_4761.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__4710_4761.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__4710_4761):map__4710_4761);
var doc_4763 = cljs.core.get.call(null,map__4710_4762__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_4764 = cljs.core.get.call(null,map__4710_4762__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println.call(null);

cljs.core.println.call(null," ",name_4760);

cljs.core.println.call(null," ",arglists_4764);

if(cljs.core.truth_(doc_4763)){
cljs.core.println.call(null," ",doc_4763);
} else {
}


var G__4765 = cljs.core.next.call(null,seq__4698_4753__$1);
var G__4766 = null;
var G__4767 = (0);
var G__4768 = (0);
seq__4698_4738 = G__4765;
chunk__4699_4739 = G__4766;
count__4700_4740 = G__4767;
i__4701_4741 = G__4768;
continue;
}
} else {
}
}
break;
}
} else {
}

if(cljs.core.truth_(n)){
var temp__5720__auto__ = cljs.spec.alpha.get_spec.call(null,cljs.core.symbol.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.ns_name.call(null,n))].join(''),cljs.core.name.call(null,nm)));
if(cljs.core.truth_(temp__5720__auto__)){
var fnspec = temp__5720__auto__;
cljs.core.print.call(null,"Spec");

var seq__4712 = cljs.core.seq.call(null,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"args","args",1315556576),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Keyword(null,"fn","fn",-1175266204)], null));
var chunk__4713 = null;
var count__4714 = (0);
var i__4715 = (0);
while(true){
if((i__4715 < count__4714)){
var role = cljs.core._nth.call(null,chunk__4713,i__4715);
var temp__5720__auto___4769__$1 = cljs.core.get.call(null,fnspec,role);
if(cljs.core.truth_(temp__5720__auto___4769__$1)){
var spec_4770 = temp__5720__auto___4769__$1;
cljs.core.print.call(null,["\n ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.name.call(null,role)),":"].join(''),cljs.spec.alpha.describe.call(null,spec_4770));
} else {
}


var G__4771 = seq__4712;
var G__4772 = chunk__4713;
var G__4773 = count__4714;
var G__4774 = (i__4715 + (1));
seq__4712 = G__4771;
chunk__4713 = G__4772;
count__4714 = G__4773;
i__4715 = G__4774;
continue;
} else {
var temp__5720__auto____$1 = cljs.core.seq.call(null,seq__4712);
if(temp__5720__auto____$1){
var seq__4712__$1 = temp__5720__auto____$1;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__4712__$1)){
var c__4319__auto__ = cljs.core.chunk_first.call(null,seq__4712__$1);
var G__4775 = cljs.core.chunk_rest.call(null,seq__4712__$1);
var G__4776 = c__4319__auto__;
var G__4777 = cljs.core.count.call(null,c__4319__auto__);
var G__4778 = (0);
seq__4712 = G__4775;
chunk__4713 = G__4776;
count__4714 = G__4777;
i__4715 = G__4778;
continue;
} else {
var role = cljs.core.first.call(null,seq__4712__$1);
var temp__5720__auto___4779__$2 = cljs.core.get.call(null,fnspec,role);
if(cljs.core.truth_(temp__5720__auto___4779__$2)){
var spec_4780 = temp__5720__auto___4779__$2;
cljs.core.print.call(null,["\n ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.name.call(null,role)),":"].join(''),cljs.spec.alpha.describe.call(null,spec_4780));
} else {
}


var G__4781 = cljs.core.next.call(null,seq__4712__$1);
var G__4782 = null;
var G__4783 = (0);
var G__4784 = (0);
seq__4712 = G__4781;
chunk__4713 = G__4782;
count__4714 = G__4783;
i__4715 = G__4784;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
} else {
return null;
}
}
});

//# sourceMappingURL=repl.js.map
